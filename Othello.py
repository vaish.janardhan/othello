BOARD_SIZE = 8
MOVE = 'M'
LEGAL_MOVES = 'L'
QUIT = 'Q'
BLACK = 'B'
WHITE = 'W'

def setupBoard():
    board = []
    for _ in range(BOARD_SIZE):
        board.append([ch for ch in input()])
    return board

def displayBoard(board):
    for row in board:
        for cell in row:
            print(cell, end = ' ')
        print()
        
def getPossibleMoves(board, player):
    for row in range(BOARD_SIZE):
        for col in range BOARD_SIZE:
            
def changePlayer(current_player):
    return BLACK if current_player is WHITE else WHITE            
                
def makeMove(player, board):
    return board        

def playOthello():
    board = setupBoard()
    current_player = input()
    more_commands = True
    while more_commands:
        command = input()
        if command[0] == MOVE:
            makeMove(current_player, board)
            current_player = changePlayer(current_player)
        elif command[0] == LEGAL_MOVES:
            getPossibleMoves(board, current_player)
            print('legal')
        elif command[0] == QUIT:
            displayBoard(board)
            more_commands = False

print(changePlayer('B'))